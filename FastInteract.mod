<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="FastInteract" version="0.31" date="01/21/2009" >
		<Author name="Vidhar" email="-" />
		<Description text="Shift+Rightclick on a NPC will provide the following features: questgivers: accept one quest immediatly. Healer: heal all injuries" />
	    <Dependencies>
	        <Dependency name="EA_InteractionWindow" />
	        <Dependency name="EA_UiModWindow" />
            <Dependency name="LibSlash" />
	    </Dependencies>
		<Files>
			<File name="FastInteract.lua" />
			<File name="FastInteract.xml" />
		</Files>
		<OnInitialize>
			<CallFunction name="FastInteract.Initialize" />
		</OnInitialize>
		<OnUpdate/>
		<OnShutdown>
			<CallFunction name="FastInteract.OnShutdown" />
		</OnShutdown>
		<SavedVariables>
			<SavedVariable name="FastInteractOptions" />
		</SavedVariables>
	</UiMod>
</ModuleFile>
